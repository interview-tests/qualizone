import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AuthInterceptor } from './helpers/jwt.interceptor';

import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppRoutingModule } from './app-routing.module';
import { HttpClientModule } from '@angular/common/http';
import { LoginComponent } from './login/login.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { MatDialogModule, MatDialog, MatDialogRef } from '@angular/material/dialog';
import { NgBootstrapFormValidationModule } from 'ng-bootstrap-form-validation';
import { ListComponent } from './form-user/list/list.component';
import { AddEditComponent } from './form-user/add-edit/add-edit.component';
import { NgxMatSelectSearchModule } from 'ngx-mat-select-search';
import { MatSelectModule } from '@angular/material/select';
import { MatFormFieldModule } from '@angular/material/form-field';
import { SelectDropDownModule } from 'ngx-select-dropdown'
import { TextSearchPipe } from './helpers/searchable';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    ListComponent,
    AddEditComponent, TextSearchPipe
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    MatFormFieldModule,
    MatSelectModule,
    SelectDropDownModule,
    MatDialogModule,
    AppRoutingModule,
    ReactiveFormsModule,
    NgxMatSelectSearchModule,
    BrowserAnimationsModule,
    NgBootstrapFormValidationModule.forRoot(),
  ],
  entryComponents: [
    AddEditComponent
  ],
  providers: [{ provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true }],
  bootstrap: [AppComponent]
})
export class AppModule { }
