import { Component, OnInit, Inject } from '@angular/core';
import { FormControl, FormGroup, Validators } from "@angular/forms";
import { FormService } from '../../services/form.service';
import { FormModel } from '../FormModel';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'app-add-edit',
  templateUrl: './add-edit.component.html',
  styleUrls: ['./add-edit.component.css']
})
export class AddEditComponent implements OnInit {
  searchText = '';
  countries = [
    { name: "Lebanon" },
    { name: "Italy" },
    { name: "Spain" },
    { name: "Syria" },
  ];

  formGroup: FormGroup = new FormGroup({
    email: new FormControl('', [
      Validators.required,
      Validators.pattern(/^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/)
    ]),
    first_name: new FormControl('', [
      Validators.required,
    ]),
    last_name: new FormControl('', [
      Validators.required,
    ]),
    mobile_number: new FormControl('', [
      Validators.required,
      Validators.minLength(8),
      Validators.maxLength(8)
    ]),
    country: new FormControl('', [
      Validators.required,
    ]),
    subject: new FormControl('', [
      Validators.required,
    ]),
    message: new FormControl('', [
      Validators.required,
    ]),
  });;

  form = this.datasForm.formData;
  title = this.datasForm.title;
  loading: boolean = false;
  disabled: boolean = false;

  constructor(private formService: FormService,
    @Inject(MAT_DIALOG_DATA) public datasForm: FormModel, private dailogRef: MatDialogRef<AddEditComponent>) { }

  ngOnInit(): void {
    if (this.form['deleted_at'] != null) {
      this.disabled = true;
      this.formGroup.disable();
    } else {
      this.formGroup.enable();
      this.disabled = false;
    }
  }

  onSubmit() {
    if (!this.form['id']) {
      this.form['mobile_number'] = this.form['mobile_number'] + '';
      this.formService.create(this.form).subscribe(data => {
        this.dailogRef.close();
      })
    } else {
      this.form['mobile_number'] = this.form['mobile_number'] + '';
      this.formService.update(this.form['id'], this.form).subscribe(data => {
        this.dailogRef.close();
      })
    }
  }

}
