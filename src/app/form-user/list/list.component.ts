import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { FormService } from '../../services/form.service';
import { MatDialog } from '@angular/material/dialog';
import { FormModel } from '../FormModel';
import { AddEditComponent } from '../add-edit/add-edit.component';
import { AuthService } from '../../services/auth.service';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {

  first_name: string;
  middle_name: string;
  last_name: string;
  email: string;
  mobile_number: string;
  phone_number: string;
  country: string;
  address: string;
  subject: string;
  message: string;

  formArray: any;

  constructor(private authService: AuthService, private formService: FormService,
    public dialog: MatDialog, private router: Router) { }

  ngOnInit(): void {
    this.getAllForms();
  }

  getAllForms() {
    this.formService.getAllForms().subscribe(data => {
      (data);
      this.formArray = data;
    })
  }

  createForm() {
    const dialog = new FormModel('Add', {});
    const dialogRef = this.dialog.open(AddEditComponent, {
      data: dialog,
      minWidth: '500px',
      width: 'auto',
      height: 'auto',
    });
    dialogRef.afterClosed().subscribe(result => {
      this.getAllForms();
    });
  }

  editForm(formData) {
    const dialog = new FormModel('Edit', formData);
    const dialogRef = this.dialog.open(AddEditComponent, {
      data: dialog,
      minWidth: '500px',
      width: 'auto',
      height: 'auto',
    });
    dialogRef.afterClosed().subscribe(result => {
      this.getAllForms();
    });
  }

  logout() {
    this.authService.logout().subscribe(
      data => {
        this.router.navigateByUrl('login');
        localStorage.removeItem('api_token');
      },
      error => {
        console.log(error);
      });
  }

  delete(formId) {
    this.formService.delete(formId).subscribe(
      data => {
        this.getAllForms();
      },
      error => {
        console.log(error);
      });
  }



}
