import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { AuthService } from '../services/auth.service';
import { AlertService } from '../services/alert.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup = new FormGroup({
    email: new FormControl('', [
      Validators.required,
      Validators.pattern(/^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/)
    ]),
    password: new FormControl('', [
      Validators.required,
      Validators.minLength(8),
      Validators.maxLength(20)
    ])
  });

  loading = false;
  submitted = false;
  returnUrl: string;
  user: any;

  constructor(
    private router: Router,
    private authService: AuthService,
    private alertService: AlertService,
  ) {
    this.user = {};
    if (this.authService.currentUserValue) {
      this.router.navigate(['/']);
    }
  }
  ngOnInit(): void {
  }

  onSubmit() {

    this.submitted = true;
    this.alertService.clear();
    this.loading = true;

    let userData = {
      email: this.user.email,
      password: this.user.password
    }

    this.authService.login(userData)
      .subscribe(
        data => {
          localStorage.setItem('api_token', data['token']);
          this.router.navigateByUrl('list');
        },
        error => {
          console.log(error);
          this.alertService.error(error);
          this.loading = false;
        });
  }

}

