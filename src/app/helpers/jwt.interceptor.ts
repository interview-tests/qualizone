import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent, HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';

@Injectable()

export class AuthInterceptor implements HttpInterceptor {

  api_url = environment.api_url;

  constructor(
    private http: HttpClient,
  ) { }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    if (!request || !request.url || (/^http/.test(request.url) && !(this.api_url && request.url.startsWith(this.api_url)))) {
      return next.handle(request);
    }

    const token = localStorage.getItem('api_token');
    if (!token) {
      console.log("Token in AuthInterceptor", token);
    }

    if (!!token) {
      request = request.clone({
        setHeaders: {
          Authorization: 'Bearer ' + token,
        }
      });
    }
    return next.handle(request);
  }
}
