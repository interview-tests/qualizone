import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  api_url = environment.api_url;

  constructor(private http: HttpClient) { }

  currentUserValue() {
    return this.http.get(this.api_url + '/auth-user');
  }

  login(user) {
    return this.http.post(this.api_url + '/login', { email: user.email, password: user.password });
  }

  logout() {
    return this.http.post(this.api_url + '/logout', {});
  }

}
