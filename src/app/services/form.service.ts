
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class FormService {
  form_url = environment.api_url + '/forms';

  constructor(private http: HttpClient) { }

  getDeletedForms() {
    return this.http.get(this.form_url + '/deleted-forms');
  }

  getAllForms() {
    return this.http.get(this.form_url);
  }

  get(formId) {
    return this.http.get(this.form_url + '/' + formId);
  }

  create(formData) {
    return this.http.post(this.form_url, formData);
  }

  update(formId, formData) {
    return this.http.put(this.form_url + '/' + formId, formData);
  }

  delete(formId) {
    return this.http.delete(this.form_url + '/' + formId);
  }

}
